<?php
if (!isset($_SERVER['STATIC_ROOT'])) {
    $_SERVER['STATIC_ROOT'] = '/static/';
}
?>
<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8" />
    <meta name="description" content="A pontos idő megjelenítése." />
    <meta name="author" content="&copy; Pőcze Bence. 2015" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Pontos idő</title>
    <?php if (preg_match('/^(http(s)?:)?\/\//', $_SERVER['STATIC_ROOT']) === 1): ?>
        <link href="<?= $_SERVER['STATIC_ROOT'] ?>" rel="preconnect">
    <?php endif; ?>
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <?php if (!empty($_SERVER['GOOGLE_ANALITICS_ID'])): ?>
        <link href="https://www.googletagmanager.com" rel="preconnect">
        <link href="https://www.google-analytics.com" rel="preconnect">
    <?php endif; ?>
    <link href="https://fonts.googleapis.com/css2?family=Cousine:wght@400;700&amp;family=Oxygen:wght@400;700&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= $_SERVER['STATIC_ROOT'] ?>time.css" />
    <link rel="icon" href="<?= $_SERVER['STATIC_ROOT'] ?>time.png" />
</head>
<body>
    <main>
        <h1>Pontos idő</h1>
        <p class="date"><span id="year">...</span>. <span id="month">...</span> <span id="date">...</span>., <span id="day">...</span></p>
        <p class="time"><span id="time">..:..:..</span></p>
        <p id="correct">Az eszköz órájának eltérése: <span class="correct"><span id="correct_seconds">...</span> másodperc</span></p>
    </main>
    <script async src="<?= $_SERVER['STATIC_ROOT'] ?>time.js"></script>
    <?php if (!empty($_SERVER['GOOGLE_ANALITICS_ID'])): ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $_SERVER['GOOGLE_ANALITICS_ID'] ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', '<?= $_SERVER['GOOGLE_ANALITICS_ID'] ?>');
        </script>
    <?php endif; ?>
</body>
</html>
